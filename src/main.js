import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// const axios = require("axios").default;

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  router,
  render: function(h) {
    return h(App);
  },
  data: {
    users: "",
    userid: 0
  }
  // methods: {
  //   allRecords: function() {
  //     axios
  //       .get("@/api/ajaxfile.php")
  //       .then(function(response) {
  //         app.users = response.data;
  //       })
  //       .catch(function(error) {
  //         console.log(error);
  //       });
  //   },
  //   recordByID: function() {
  //     if (this.userid > 0) {
  //       axios
  //         .get("@/api/ajaxfile.php", {
  //           params: {
  //             userid: this.userid
  //           }
  //         })
  //         .then(function(response) {
  //           app.users = response.data;
  //         })
  //         .catch(function(error) {
  //           console.log(error);
  //         });
  //     }
  //   }
  // }
});

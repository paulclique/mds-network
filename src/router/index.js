import Vue from "vue";
import VueRouter from "vue-router";
import Accueil from "../views/Accueil.vue";
import Projets from "../views/Projets.vue";
import Campus from "../views/Campus.vue";
import Annonces from "../views/Annonces.vue";
import Messages from "../views/Messages.vue";
import Profil from "../views/Profil.vue";
import Parametres from "../views/Parametres.vue";
import SingleProjet from "../views/SingleProjet.vue";
import ConnexionInscription from "../views/ConnexionInscription.vue";
import PublierProjet from "../views/PublierProjet.vue";
import SingleCampus from "../views/SingleCampus.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Accueil",
    component: Accueil
  },
  {
    path: "/projets",
    name: "Projets",
    component: Projets
  },
  {
    path: "/campus",
    name: "Campus",
    component: Campus
  },
  {
    path: "/annonces",
    name: "Annonces",
    component: Annonces
  },
  {
    path: "/messages",
    name: "Messages",
    component: Messages
  },
  {
    path: "/profil",
    name: "Profil",
    component: Profil
  },
  {
    path: "/parametres",
    name: "Paramètres",
    component: Parametres
  },
  {
    path: "/single-projet",
    name: "Single Projet",
    component: SingleProjet
  },
  {
    path: "/connexion-inscription",
    name: "Connexion & inscription",
    component: ConnexionInscription
  },
  {
    path: "/publier-projet",
    name: "Publier un projet",
    component: PublierProjet
  },
  {
    path: "/single-campus",
    name: "Single Campus",
    component: SingleCampus
  }
];

const router = new VueRouter({
  routes
});

export default router;

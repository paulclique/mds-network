<?php

$host = "localhost";
$user = "root";
$password = "";
$dbname = "mdsnetwork";

$con = mysqli_connect($host, $user, $password,$dbname);
// Check connection
if (!$con) {
  die("Echec de la connexion à la BDD : " . mysqli_connect_error());
}
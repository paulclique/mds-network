<?php

/**
 * @return PDO
 */

function getPDO(): PDO {
    $servername = 'localhost';
    $dbname = 'mdsnetwork';
    $user = 'root';
    $pass = '';

    $pdo = new PDO("mysql:host=$servername;dbname:$dbname",
    $user, $pass, [
        PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ]);
    
    return $pdo;
}
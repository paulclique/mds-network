<?php

require_once (dirname(__DIR__) . '/config/database.php');

class Users {
    protected $pdo;
    
    public function __construct()
    {   
        $this->pdo = getPDO();
    }

    public function list()
    {
        $sql = $this->pdo->query("
            SELECT *
            FROM users
            ORDER BY username
            DESC
        ");

        $sql->setFetchMode(\PDO::FETCH_ASSOC);

        return $sql;
    }
}

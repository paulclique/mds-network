<?php

header('Content-Type: application/json');

require_once(dirname(__DIR__) . '/models/Users.php');

$model = new Users();
$sql = $model->list();

echo json_encode($sql->fetchAll(), JSON_PRETTY_PRINT);